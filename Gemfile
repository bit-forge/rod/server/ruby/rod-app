# frozen_string_literal: true

source 'https://rubygems.org'

ruby '3.3.4'

gem 'rails', '= 7.2.0'

gem 'puma', '= 6.4.2'

gem 'bootsnap', '= 1.18.4', require: false

gem 'bcrypt', '= 3.1.20'

# ------------------------------------------------------------------------------- DB & Active Record
gem 'pg', '= 1.5.7'

# ------------------------------------------------------------------------------------------- Assets
gem 'cssbundling-rails', '= 1.4.1'
gem 'jsbundling-rails', '= 1.3.1'
gem 'sprockets-rails', '= 3.5.2'

# ------------------------------------------------------------------------------ Controllers helpers
gem 'jbuilder', '= 2.12.0'

# ------------------------------------------------------------------------------------- View Helpers
gem 'view_component', '= 3.13.0'

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[windows jruby]

group :development, :test do
  # See https://guides.rubyonrails.org/debugging_rails_applications.html#debugging-with-the-debug-gem
  gem 'debug', '= 1.9.2', platforms: %i[mri windows]
end

group :development do
  gem 'lookbook', '= 2.3.2'
  # Use console on exceptions pages [https://github.com/rails/web-console]
  gem 'web-console'

  # Add speed badges [https://github.com/MiniProfiler/rack-mini-profiler]
  # gem "rack-mini-profiler"

  # Speed up commands on slow machines / big apps [https://github.com/rails/spring]
  # gem "spring"

  gem 'faker', '= 3.4.2'
end

group :test do
  # Use system testing [https://guides.rubyonrails.org/testing.html#system-testing]
  # gem 'capybara'
  # gem 'selenium-webdriver', '= 4.20.1'
  gem 'rspec', '= 3.13.0'
  gem 'rspec-rails', '= 6.1.3'
end

# --------------------------------------------------------------------------------------- ruby_check
group :ruby_check do
  gem 'brakeman', '= 6.1.2', require: false
  gem 'erb_lint', '0.6.0', require: false
  gem 'rubocop', '= 1.65.1', require: false
  gem 'rubocop-performance', '= 1.21.1', require: false
  gem 'rubocop-rails', '= 2.25.1', require: false
  gem 'rubocop-rspec', '= 3.0.4', require: false
  gem 'rubocop-rspec_rails', '= 2.30.0', require: false
end
