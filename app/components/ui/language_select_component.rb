# frozen_string_literal: true

module UI
  class LanguageSelectComponent < ApplicationComponent
    # @return [String]
    def language
      I18n.locale.to_s.upcase
    end

    # @return [Array<String>]
    def languages
      I18n.available_locales.map(&:to_s)
    end
  end
end
