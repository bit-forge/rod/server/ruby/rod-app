# frozen_string_literal: true

User.delete_all

User.create!(
  username: 'admin',
  locale: 'en',
  password: '123456',
  password_confirmation: '123456'
)
