# frozen_string_literal: true

module ApplicationHelper
  # @return [PageViewObject]
  def page_view_object
    @page_view_object ||= ::PageViewObject.new(controller_path:, action_name:)
  end
end
