// Entry point for the build script in your package.json
import Rails from '@rails/ujs';
import { initFlashMessages } from "./init_flash_messages";
// import { Dropdown } from 'bootstrap';

Rails.start();

document.addEventListener('DOMContentLoaded', () => {
  initFlashMessages();

  // const dropdownElementList = document.querySelectorAll('.dropdown-toggle')
  // const dropdownList = [...dropdownElementList].map(dropdownToggleEl => new Dropdown(dropdownToggleEl))

});
