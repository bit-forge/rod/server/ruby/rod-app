.PHONY: db_reset db-reset run erblint rubocop test ok
.SILENT: db_reset db-reset run erblint rubocop test ok

db-reset: db_reset

db_reset:
	bin/db_reset

run:
	bin/rails server -b 0.0.0.0


erblint:
	bin/erblint

rubocop:
	bin/rubocop

test:
	bin/test

ok: erblint rubocop test
