import { Toast } from 'bootstrap';

export function initFlashMessages() {
  let flashMessages = document.querySelector('#flash-messages');

  if (flashMessages) {
    flashMessages.querySelectorAll('.toast').forEach((toastElement) => {
      const toast = Toast.getOrCreateInstance(toastElement);

      toastElement.addEventListener('hide.bs.toast', () => {
        toastElement.remove();
      });

      toast.show();
    });
  }
}
