# frozen_string_literal: true

module Users
  module ControllerHelpers
    module CurrentUser
      extend ActiveSupport::Concern

      included do
        before_action :load_current_user

        helper_method :current_user
      end

      private

      def load_current_user
        @current_user = user_from_session
      end

      def user_from_session
        return nil if session[:user_id].blank?

        User.find_by(id: session[:user_id])
      end

      def current_user
        @current_user
      end
    end
  end
end
