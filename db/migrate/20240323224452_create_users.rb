class CreateUsers < ActiveRecord::Migration[7.1]
  def change
    create_table :users do |t|
      t.string :username, null: false, index: { unique: true }
      t.string :password_digest, null: false

      t.string :locale, limit: 2, default: 'en'

      t.timestamps
    end
  end
end
