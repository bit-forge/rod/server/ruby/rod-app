# frozen_string_literal: true

module UI
  module Header
    class UserMenuComponent < ApplicationComponent
      # @param user [User]
      def initialize(user: nil)
        @user = user
      end

      def render?
        @user.present?
      end

      # @return [String]
      def username
        @user.username
      end
    end
  end
end
