# frozen_string_literal: true

module ActionsLocalizer
  extend ActiveSupport::Concern

  included do
    around_action :use_locale
  end

  private

  def use_locale(&)
    locale = params[:locale].presence || :en
    locale = :en unless I18n.available_locales.include?(locale.to_sym)
    I18n.with_locale(locale, &)
  end
end
