# frozen_string_literal: true

class ApplicationController < ActionController::Base
  include ActionsLocalizer
  include Users::ControllerHelpers::CurrentUser

  add_flash_types :info, :error, :success, :warning
  default_form_builder BootstrapFormBuilder

  before_action :authenticate_user!

  private

  def authenticate_user!
    return if current_user

    redirect_to new_users_session_path, error: t('users.sessions.must_be_logged_in')
  end
end
