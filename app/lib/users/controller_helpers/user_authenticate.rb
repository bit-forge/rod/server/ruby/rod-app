# frozen_string_literal: true

module Users
  module ControllerHelpers
    module UserAuthenticate
      def user_authenticate?(user, password)
        return false if user.blank? || password.blank?

        user.authenticate(password) && session[:user_id] = user.id
      end
    end
  end
end
