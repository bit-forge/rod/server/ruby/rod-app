# frozen_string_literal: true

module Users
  class SessionsController < ::ApplicationController
    include Users::ControllerHelpers::UserAuthenticate

    layout 'centered'

    skip_before_action :authenticate_user!

    def new
      @user = User.new
    end

    # rubocop:disable Metrics/AbcSize
    def create
      if user_authenticate?(User.find_by(username: user_params[:username]), user_params[:password])
        redirect_to root_path
      else
        flash[:error] = t(
          '.invalid_credentials',
          username: t('activerecord.attributes.user.username'),
          password: t('activerecord.attributes.user.password')
        )

        redirect_to new_users_session_path(locale: params[:locale])
      end
    end
    # rubocop:enable Metrics/AbcSize

    def destroy
      session[:user_id] = nil

      redirect_to new_users_session_path, success: t('users.sessions.logged_out')
    end

    private

    def user_params
      params.require(:user).permit(:username, :password)
    end
  end
end
