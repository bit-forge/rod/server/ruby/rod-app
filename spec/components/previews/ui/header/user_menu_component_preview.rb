# frozen_string_literal: true

module UI
  module Header
    # @display center true
    class UserMenuComponentPreview < ViewComponent::Preview
      def default
        render(UI::Header::UserMenuComponent.new(user: User.new(username: 'username')))
      end
    end
  end
end
