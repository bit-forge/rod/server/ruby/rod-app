# frozen_string_literal: true

scope :users, module: :users, as: :users do
  resource :session, only: %i[new create destroy]
end
