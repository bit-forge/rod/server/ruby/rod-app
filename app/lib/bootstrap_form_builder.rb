# frozen_string_literal: true

class BootstrapFormBuilder < ActionView::Helpers::FormBuilder
  def label(method, text = nil, options = {}, &)
    merge_class(options, 'form-label')
    @template.label(@object_name, method, text, objectify_options(options), &)
  end

  def text_field(method, options = {})
    merge_class(options, 'form-control')
    @template.text_field(@object_name, method, options)
  end

  def password_field(method, options = {})
    merge_class(options, 'form-control')
    @template.password_field(@object_name, method, options)
  end

  def submit(value = nil, options = {})
    if value.is_a?(Hash)
      options = value
      value = nil
    end
    value ||= submit_default_value
    merge_class(options, 'btn btn-primary')
    @template.submit_tag(value, options)
  end

  private

  def merge_class(options, classes)
    if options[:class]
      options[:class] += " #{classes}"
    else
      options[:class] = classes
    end
  end
end
