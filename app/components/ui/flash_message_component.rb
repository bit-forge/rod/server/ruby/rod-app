# frozen_string_literal: true

module UI
  class FlashMessageComponent < ApplicationComponent
    TYPES = %i[info error success].freeze

    BASE_CLASSES = {
      info: 'border-info',
      error: 'border-danger',
      success: 'border-success',
      warning: 'border-warning'
    }.freeze

    HEADER_CLASSES = {
      info: 'bg-info border-info text-info',
      error: 'bg-danger border-danger text-danger',
      success: 'bg-success border-success text-success',
      warning: 'bg-warning border-warning text-warning'
    }.freeze

    ICON_CLASSES = {
      info: 'bi-info-circle-fill',
      error: 'bi-x-circle-fill',
      success: 'bi-check-circle-fill',
      warning: 'bi-exclamation-triangle-fill'
    }.freeze

    MESSAGE_CLASSES = {
      info: 'text-info-emphasis',
      error: 'text-danger-emphasis',
      success: 'text-success-emphasis',
      warning: 'text-warning-emphasis'
    }.freeze

    attr_reader :message

    def initialize(type:, message:)
      @type = type.to_sym
      @message = message
    end

    def title
      I18n.t("flash.#{@type}.title")
    end

    def icon
      tag.i(class: "fs-5 me-2 bi #{ICON_CLASSES[@type]}")
    end

    def base_tag_class
      "toast border-opacity-50 bg-white #{BASE_CLASSES[@type]}"
    end

    def header_tag_class
      "toast-header border-opacity-50 bg-opacity-25 #{HEADER_CLASSES[@type]}"
    end

    def message_tag_class
      "toast-body #{MESSAGE_CLASSES[@type]}"
    end
  end
end
