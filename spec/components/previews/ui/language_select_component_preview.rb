# frozen_string_literal: true

module UI
  # @display center true
  # @display bg_color "#000000"
  class LanguageSelectComponentPreview < ViewComponent::Preview
    def default
      render(UI::LanguageSelectComponent.new)
    end
  end
end
