# frozen_string_literal: true

class PageViewObject
  # @param controller_path [String]
  # @param action_name [String]
  def initialize(controller_path:, action_name:)
    @controller_path = controller_path
    @action_name = action_name
  end

  # @return [String]
  def app_name
    'Rod'
  end

  # @return [String]
  def app_version
    RodApp::VERSION
  end

  # @return [String]
  def title
    @title ||= "#{app_name} | #{l_title}"
  end

  # @return [String]
  def main_title
    @main_title ||= l_title
  end

  # @return [String]
  def app_title
    "#{app_name}: #{app_version}#{Rails.env.production? ? '' : ' (dev)'}"
  end

  private

  def controller_path
    @controller_path.tr('/', '.')
  end

  # @return [String]
  def l_title
    I18n.t(
      "page_view_object.#{controller_path}.#{@action_name}.title",
      default: [
        :"page_view_object.#{controller_path}.title",
        'Page'
      ]
    )
  end
end
