# frozen_string_literal: true

Faker::Config.locale = 'en-US'

ApplicationRecord.class_eval do
  after_save :log_to_console
  after_destroy :log_to_console

  private

  def log_to_console
    db_action = created_at == updated_at ? 'Created' : 'Updated'
    db_action = 'Destroyed' if destroyed?

    puts "#{db_action} #{self.class.name} <id: #{id}>" # rubocop:disable Rails/Output
  end
end

require_relative 'seeds/users'
