# frozen_string_literal: true

module UI
  # @display max_width 300px
  class FlashMessageComponentPreview < ViewComponent::Preview
    # @param locale select {{ locales }} "The theme to use for the button"
    # @!group All Flashes
    def info(locale: :en)
      use_locale(locale)
      render(UI::FlashMessageComponent.new(type: :info, message: 'This is an info message'))
    end

    def error(locale: :en)
      use_locale(locale)
      render(UI::FlashMessageComponent.new(type: :error, message: 'This is an error message'))
    end

    def success(locale: :en)
      use_locale(locale)
      render(UI::FlashMessageComponent.new(type: :success, message: 'This is a success message'))
    end

    def warning(locale: :en)
      use_locale(locale)
      render(UI::FlashMessageComponent.new(type: :warning, message: 'This is a warning message'))
    end
    # @!endgroup

    private

    def use_locale(locale)
      I18n.locale = locale # rubocop:disable Rails/I18nLocaleAssignment
    end

    def locales
      %i[en uk ru]
    end
  end
end
