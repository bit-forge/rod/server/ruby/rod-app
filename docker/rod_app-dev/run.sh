#!/bin/sh

bundle install
bundle clean --force

rails db:create
rails db:migrate

yarn install

rm tmp/pids/server.pid

sh bin/dev
